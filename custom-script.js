

$('.lang-link').attr('href', function (index, value) {
    return value + window.location.search;
});

// When the user scrolls the page, execute myFunction 

window.onscroll = function () { myFunction() };

$('.section-link').on("click", (event) => {
    $('li.active-link').removeClass('active-link');
    $(event.target).parent().addClass('active-link');
});
function myFunction() {
    var winScroll = document.body.scrollTop || document.documentElement.scrollTop;

    if (winScroll < document.querySelector("header").clientHeight) {
        document.querySelector(".toc-container").style.position = "";
        $("#logo-left").hide();
    } else {
        document.querySelector(".toc-container").style.position = "fixed";
        $("#logo-left").show();
    }


}



const urlParams = new URLSearchParams(window.location.search);
const cat = urlParams.get('cat');
if (cat !== 'package-seller') {
    $('#left-panel').hide();
    $('#first').hide();
    $('#second').hide();
    $('#third').hide();
    $('#forth').hide();
    $('#sixth').hide();
    $('#right-panel').removeClass('col-7');
    $('#right-panel .col-2').hide();
    $('#right-panel').addClass('container');
}